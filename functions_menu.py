option_selected = -1


def english():
    return 'Hello!'


def spanish():
    return '¡Hola!'


def portuguese():
    return 'Olá!'


def french():
    return 'Salut!'


def incorrect_option():
    return 'Choose a correct option'


while option_selected != 0:
    print("""
________________________________________________________________________________________________________________________

Menú para darte un saludo
    Opción 1. Saludo en inglés
    Opción 2. Saludo en español
    Opción 3. Saludo en portugués
    Opción 4. Saludo en francés
    """)
    option_selected = int(input('Ingresa el número del saludo que deseas recibir\t'))
    if option_selected == 1:
        print(english())
    elif option_selected == 2:
        print(spanish())
    elif option_selected == 3:
        print(portuguese())
    elif option_selected == 4:
        print(french())
    else:
        print(incorrect_option())


def logout():
    return '¡Adiós!'


print(logout())

