"""
Ejercicios de práctica
"""

"""
1. Codificar un programa que muestre los números impares entre 1 y 99 y que diga cuántos son
"""
counter_odd = 0
for n in range(1, 100):
    if n % 2 != 0:
        print(n, end=" ")
        counter_odd += 1
print(f'\nLos números impares entre 1 y 99 son {counter_odd}')

print('-------------------------------------------------------------------------------------------------------------')

"""
2. Codificar un programa que muestre los números pares entre 2 y 100 y que diga cuántos son
"""
counter_even = 0
for n in range(2, 101):
    if n % 2 == 0:
        print(n, end=" ")
        counter_even += 1
print(f'\nLos números pares entre 2 y 100 son {counter_even}')

print('-------------------------------------------------------------------------------------------------------------')

"""
3. Codificar un programa muestre los múltiplos de 13 comprendidos entre los números 13 y 100 y que diga cuántos son
"""
counter_13 = 0
for n in range(13, 101, 13):
    print(n, end=" ")
    counter_13 += 1
print(f'\nLos múltiplos de 13 entre 13 y 100 son {counter_13}')

print('------------------------------------------------')

number = int(input('Ingrese el número del cual desea calcular su factorial -->\t'))
factorial = 1
for n in range(number, 0, -1):
    factorial *= n
print(f'El factorial de {number} es {factorial}')

print('-------------------------------------------------------------------------------------------------------------')

"""
Codificar un programa que lea dos números num1 y num2, donde num1 > num2. Calcular y mostrar el factorial de todos los
números comprendidos entre num1 y num2 incluyéndolos.
"""
print("""
Vamos a leer dos números y a calcular el factorial de los números comprendidos entre ellos. 
Recuerda que el primer número debe ser mayor que el segundo
""")
number1 = int(input('Ingrese el primer número -->\t'))
number2 = int(input('Ingrese el segundo número -->\t'))
factorial_between_2n = 1
for n in range(number1, number2-1, -1):
    factorial_between_2n *= n
print(f'El factorial entre {number1} y {number2} es {factorial_between_2n}')

print('-------------------------------------------------------------------------------------------------------------')

"""
El número de combinaciones de m elementos, tomados en grupos de n elementos, viene dado por la fórmula que se muestra 
a continuación. Codificar un programa que lea m y n, y luego calcule y muestre C.
C = m_factorial / (n_factorial * m_minus_n_factorial)
"""
print("Vamos a calcular el número de combinaciones de \'m\' elementos tomados en grupos de \'n\' elementos")
m = int(input('Ingresa el número del conjunto m de elementos -->\t'))
n = int(input('Ingresa el número de grupos n que deseas conformar -->\t'))

# Cálculo de m factorial
m_factorial = 1
for i in range(m, 0, -1):
    m_factorial *= i

# Cálculo de n factorial
n_factorial = 1
for i in range(n, 0, -1):
    n_factorial *= i

# Cálculo de m - n factorial
m_minus_n_factorial = 1
for i in range(m - n, 0, -1):
    m_minus_n_factorial *= i

# Aplicamos la fórmula
C = int(m_factorial / (n_factorial * m_minus_n_factorial))

print(f'El número de combinaciones de {m} elementos tomados en grupos de {n} elementos es {C}')

print('-------------------------------------------------------------------------------------------------------------')

"""
1. Se requiere un programa que lea un número de cualquier número de dígitos y que averigüe y muestre:
    1. Cuántas cifras tenías el número leído
    2. La suma de sus dígitos
    3. Número invertido
"""
print("""
Este programa lee un número y realiza estas 3 funcionalidades:
    1. Cuántas cifras tenía el número leído
    2. La suma de sus dígitos
    3. Número invertido
""")
number_to_operate = str(input('Ingrese el número -->\t'))

# Cálculo de las cifras del número leído
counter_number = 0
for n in number_to_operate:
    counter_number += 1
print(f'El número de cifras del número {number_to_operate} es {counter_number}')

# Cálculo de la suma de sus dígitos
counter_sum = 0
for n in number_to_operate:
    counter_sum += int(n)
print(f'La suma de los dígitos del número {number_to_operate} es {counter_sum}')

# Reverso del número
reverse = ""
for n in number_to_operate:
    reverse = n + reverse
print(f'El número {number_to_operate} invertido es {reverse}')

print('-------------------------------------------------------------------------------------------------------------')

"""
Codificar un programa que averigüe la serie de Fibonacci: 0 1 1 2 3 5 8 13. Mostrar los términos de la serie menores 
de 1000 y decir cuántos son
"""
print('Serie de Fibonacci')
fibonacci = 0
fibonacci1 = 0
fibonacci2 = 1
while fibonacci <= 1000:
    fibonacci = fibonacci1 + fibonacci2
    temp1 = fibonacci1
    temp2 = fibonacci2
    fibonacci2 = fibonacci1
    fibonacci1 = fibonacci
    if fibonacci1 <= 1000:
        print(f'{temp1} + {temp2} = {fibonacci}')

print('-------------------------------------------------------------------------------------------------------------')

"""
Se requiere un programa que permita hacer conversiones entre diferentes tipos de unidades de medida de acuerdo a la 
siguiente tabla:
1. Longitud
2. Peso
3. Tiempo
"""

# Solución en script conversion.py

print('-------------------------------------------------------------------------------------------------------------')


"""
Codifique un programa que calcule el tiempo de encuentro de 2 vehículos que van en sentido opuesto, teniendo como datos 
la distancia inicial que los separa y la velocidad de cada uno.
"""
print('Este programa calcula el tiempo de encuentro de 2 vehículo9s en sentido opuesto')
speed_car1 = int(input('Ingrese la velocidad del vehículo 1 (km/h) -->\t'))
speed_car2 = int(input('Ingrese la velocidad del vehículo 2 (km/h) -->\t'))
distance = int(input('Ingrese la distancia entre los dos vehículos (km) -->\t'))
time = distance / (speed_car1 + speed_car2)
print(f'El tiempo de encuentro entre el vehículo 1 que va a una velocidad de {speed_car1} km/h y el vehículo 2 que va '
      f'a una velocidad de {speed_car2} km/h es de {time} hora(s)')

print('-------------------------------------------------------------------------------------------------------------')

"""
Codifique un programa que calcule el precio de un televisor si el proveedor ofrece un descuento del 10% sobre el precio 
sin IVA si éste cuesta $1.000.000 o más. Además, independientemente de esto, ofrece un 5% de descuento adicional 
sobre el precio si la marca es “LG”. Siendo las marcas vendidas en el almacén: LG, SAMSUNG, SONY, PANASONIC
"""

# Solución en script television.py