"""Este programa pide al usuario que ingrese su contraseña
Verifica si la contraseña tiene una longitud mínima de 10 caracteres, si es así pasa a verificar si la contraseña es
correcta."""
password = input('Ingresa tu contraseña\n')
if len(password) >= 10:
    print('Tu contraseña cumple con la longitud mínima')
    if password == 'MiSuperContraseñaSegura':
        print('Tu contraseña es correcta :) ¡Bienvenido!')
    else:
        print('Tu contraseña es incorrecta :( Vuelve a intentarlo')
else:
    print('Tu contraseña no cumple con la longitud mínima')
