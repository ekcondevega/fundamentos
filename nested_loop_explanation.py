print('Así funcionan los ciclos anidados')
for n in range(0, 3):
    print('Ciclo externo', n)
    for m in range(1, 6):
        print('Ciclo interno', m)
        print(n, '*', m, '=', n * m)
