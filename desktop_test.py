a = 5
b = 4
c = 1
a = a + b
b = a - c
c = b / c
c = a + b
a = b * a
print(a, b, c)

a = True
b = False
c = not a
a = a + b
c = a and b
a = c or c
b = b or not c
print(a, b, c)

a = 10
b = 30
c = 10
a = b / c
p1 = True
p2 = False
p3 = True
b = b + a
c = c - a
p3 = p1 and p2
user = 'usuario'
p1 = p1 or p2
domain = '@unac.edu.co'
p2 = p2 and not p3
email = user + domain
print(a, b, c, p1, p2, p3, user, domain, email)

x = 10.0
y = -30.0
z = 10.5
z = z - y
z = z**2
x = x + y + z
var1 = False
var2 = True
var1 = var1 or not var2
address = 'Cra 84 # 33AA – 1'
street = 'La Castellana'
location = address + street
print(x, y, z, var1, var2, address, street, location)
