import random
# Simulación de lanzamiento de dados

# Conteo de caras
freq1 = 0
freq2 = 0
freq3 = 0
freq4 = 0
freq5 = 0
freq6 = 0

random.seed(32)
for roll in range(10):
    face = random.randrange(1, 7)
    print(face, end=' ')
    if face == 1:
        freq1+= 1
    elif face == 2:
        freq2 += 1
    elif face == 3:
        freq3 += 1
    elif face == 4:
        freq4 += 1
    elif face == 5:
        freq5 += 1
    elif face == 6:
        freq6 += 1

print(f'\nCara 1: {freq1}\nCara 2: {freq2}\nCara 3: {freq3}\nCara 4: {freq5}\nCara 5: {freq5}\nCara 6: {freq6}')
