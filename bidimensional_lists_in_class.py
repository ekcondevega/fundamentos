students_grades = [[77, 68, 86, 73], [96, 87, 89, 81], [70, 90, 86, 81]]
print(students_grades)
# Longitud de la lista completa
print('Longitud de la lista bidimensional = ', len(students_grades))
# Longitud de una fila de la lista bidimensional
print('Longitud de la fila 0 =', len(students_grades[0]))
# Acceder a un elemento de la lista bidimensional
print('Elemento de la posición [2][3] = ', students_grades[2][3])
# Acceder a un elemento de la lista bidimensional con índices negativos
print('Elemento de la posición [2][3] accediendo con índice negativo = ', students_grades[2][-1])

print('******************************************************************************************')

# for row in students_grades = para cada fila en la lista students_grades
# Itera 3 veces porque tenemos 3 filas
for row in students_grades:
    # for item in row = para cada elemento dentro de la fila
    # Itera 4 veces porque tenemos 4 columnas o 4 elementos dentro de cada fila
    for item in row:
        # print(item, end=' ') --> Imprime cada elemento de la fila de manera horizontal
        # end=' ' --> significa que dejo un espacio en blanco entre cada elemento
        print(item, end=' ')
    # Hacer un salto de línea al terminar la fila
    print()

print('******************************************************************************************')

# Usamos enumerate para poder contar las posiciones. enumerate itera desde 0 hasta la longitud de la lista
# Añadimos i y j que nos permiten imprimir los índices
for i, row in enumerate(students_grades):
    for j, item in enumerate(row):
        # Imprime con formato para mostrar el índice
        print(f'a[{i}][{j}]={item} ', end=' ')
    print()

print('******************************************************************************************')

# Usamos in range(len(students_grades)) para determinar un rango para el for --> (0, longitud de la lista)
# longitud de la lista = número de filas
for i in range(len(students_grades)):
    # (0, longitud de la fila actual)
    # students_grades[i] --> i va cambiando de valor desde 0 hasta la longitud de la lista
    for j in range(len(students_grades[i])):
        print(f's_g[{i}][{j}]={students_grades[i][j]} ', end=' ')
    print()

print('******************************************************************************************')

# Inicializamos una lista bidimensional con 3 filas
numbers = [[], [], []]
# Defino el for en un rango de 0 - 3 --> índices 0, 1, 2
for i in range(0, 3):
    # Defino la cantidad de elementos dentro de cada fila
    # Este for itera en índices 0, 1
    for j in range(0, 2):
        # Uso el .append dentro de la fila para añadir el elemento a la fila actual
        numbers[i].append(int(input(f'Ingrese el número de la posición [{i}][{j}]')))
print(numbers)

print('******************************************************************************************')

# Inicializamos una lista vacía
numbers_n = []
# rows permite al usuario ingresar el número de filas de la lista
rows = int(input('Ingrese el número de filas'))
# cols permite al usuario ingresar el número de columnas de la lista
cols = int(input('Ingrese el número de columnas'))
# Defino el rango como la variable rows, es decir, la longitud de las filas como el número que ingresó el usuario
for row in range(rows):
    # Añado una fila vacía las veces que se haya definido mediante rows
    numbers_n.append([])
    # Defino el rango como la variable cols
    # Es decir, itero dependiendo del número de columnas que definió el usuario
    for col in range(cols):
        # Pido al usuario el valor a guardar
        num = int(input(f'Ingrese el número de la posición [{row}][{col}]'))
        # Añado a la fila el valor que el usuario digita
        numbers_n[row].append(num)
print(numbers_n)

print('******************************************************************************************')




