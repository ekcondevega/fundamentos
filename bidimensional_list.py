students_grades = [[77, 68, 86, 73], [96, 87, 89, 81], [70, 90, 86, 81]]
print(students_grades)




















# Imprimir la lista bidimensional por filas
for row in students_grades:
    for item in row:
        print(item, end=' ')
    print()
print('*********************************************************************')
# Imprimir la lista bidimensional con información de las posiciones
for i, row in enumerate(students_grades):
    for j, item in enumerate(row):
        print(f's_g[{i}][{j}]={item} ', end=' ')
    print()
print('*********************************************************************')
# Imprimir la lista bidimensional usando i, j y la función len
for i in range(len(students_grades)):
    for j in range(len(students_grades[i])):
        print(f's_g[{i}][{j}]={students_grades[i][j]} ', end=' ')
    print()
print('*********************************************************************')
# Crear una lista bidimensional usando i, j, in range e input
numbers = [[], [], []]
for i in range(0, 3):
    for j in range(0, 3):
        numbers[i].append(int(input(f'Ingrese el número de la posición [{i}][{j}]')))
print(numbers)
print('*********************************************************************')
# Crear una lista bidimensional leyendo el tamaño de las filas y las columnas
numbers_n = []
rows = int(input('Ingrese el número de filas'))
cols = int(input('Ingrese el número de columnas'))
for row in range(rows):
    numbers_n.append([])
    for col in range(cols):
        num = int(input(f'Ingrese el número de la posición [{row}][{col}]'))
        numbers_n[row].append(num)
print(numbers_n)
