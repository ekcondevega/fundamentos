# Triángulo
a = 16.16
b = 12
c = 16.16
h = 15
print(a + b + c, (b * h) / 2)

# Paralelogramo
a = 6
b = 9
h = 4
print(2 * (a + b), b * h)

# Rectángulo
a = 12
b = 18
print(2 * (a + b), b * a)

# Cuadrado
a = 13.8
print(4 * a, a ** 2)

# Rombo
a = 3.088
D = 2.7
d = 1.5
print(4 * a, (D * d) / 2)

# Cometa
a = 4.2
b = 6.2
D = 7.5
d = 3.8
print(2 * (a + b), (D * d) / 2)

# Trapecio
a = 6.1846
b = 12
B = 15
c = 6.1846
h = 6
print(a+b+B+c, ((B+b)*h)/2)

# Círculo
r = 3
print(2 * 3.14159265359 * r, 3.14159265359 * (r ** 2))
