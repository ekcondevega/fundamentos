"""
Selection statements
"""
# if
"""
Leer un número entero y determinar si es positivo
"""
number = int(input('Ingresa un número y te diré si es positivo\t'))
if number > 0:
    print('El número', number, 'es positivo')

# if else
"""
Leer un número entero y determinar si es positivo o negativo
"""
number = int(input('Ingresa un número y te diré si es positivo o negativo\t'))
if number > 0:
    print('El número', number, 'es positivo')
else:
    print('El número', number, 'es negativo')

# if elif else
"""
Leer un número entero y determinar si es positivo, igual a cero o negativo
"""
number = int(input('Ingresa un número y te diré si es positivo, igual a cero o negativo\t'))
if number > 0:
    print('El número', number, 'es positivo')
elif number < 0:
    print('El número', number, 'es negativo')
else:
    print('El número es igual a cero')


