import random
# Simulación de lanzamiento de dados
# Conteo de caras
freq1 = 0
freq2 = 0
freq3 = 0
freq4 = 0
freq5 = 0
freq6 = 0

dice1 = []
dice2 = []
freq_even = 0
for roll in range(10):
    # Dado 1
    face_dice1 = random.randrange(1, 7)  # 1 2 3 4 5 6
    dice1.append(face_dice1)
    # Dado 2
    face_dice2 = random.randrange(1, 7)  # 1 2 3 4 5 6
    dice2.append(face_dice2)

    if face_dice1 % 2 == 0 and face_dice2 % 2 == 0:
        freq_even += 1

"""
    if face == 1:
        freq1 += 1
    elif face == 2:
        freq2 += 1
    elif face == 3:
        freq3 += 1
    elif face == 4:
        freq4 += 1
    elif face == 5:
        freq5 += 1
    else:
        freq6 += 1
"""
#print(f'\nCara 1: {freq1}\nCara 2: {freq2}\nCara 3: {freq3}\n'
 #     f'Cara 4: {freq4}\nCara 5: {freq5}\nCara 6: {freq6}')
