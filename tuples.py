student_tuple = ('Natasha', 'Castle', 5.0)
print(student_tuple)
print(student_tuple[0])
print(len(student_tuple))
# student_tuple[0] = 'Nata'
print('----------------------------------------------------------')
student2_tuple = ('Alison', 'Sea', 4.5)
print(student2_tuple)
print(student2_tuple[0])
print(len(student2_tuple))
firstName, lastName, grade = student2_tuple
print(firstName)
print(lastName)
print(grade)
print('----------------------------------------------------------')
single_element_tuple = ('Natasha',)
print(single_element_tuple)
string_element = 'Natasha'
print(string_element)

print('-----------------------------------------------------------')
time_tuple = (9, 16, 1)
print(time_tuple[0])
time1, time2, time3 = time_tuple
print(time1, time2, time3)
