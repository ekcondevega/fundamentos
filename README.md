## Repositorio Fundamentos de Programación

En este repositorio encontrará los ejercicios desarrollados en clase. 

Puede abrir cada archivo en el navegador o puede clonar el repositorio a su computador mediante Git Bash con el comando

```
git clone https://gitlab.com/ekcondevega/fundamentos.git
```
