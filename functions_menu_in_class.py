# Definimos las funciones a utilizar dentro de nuestro programa
def spanish():
    return '¡Hola!'


def english():
    return 'Hello!'


def portuguese():
    return 'Olá'


def french():
    return 'Salut'


def incorrect_option():
    return 'Selecciona una opción válida'


# Variable para guardar la opción que seleccione el usuario
option_selected = 0
# Definimos un ciclo while para iterar las veces que deseemos
while option_selected != 5:
    # Definimos el menú para el usuario
    print("""
    Este programa te saluda en diferentes idiomas:
    1. Español 
    2. Inglés
    3. Portugués
    4. Francés
    5. Salir
    """)
    # Leyendo la opción. Aquí el usuario ingresa la opción que desea
    option_selected = int(input('Selecciona el idioma'))
    # Casos. Dentro de cada caso llamamos a la función
    if option_selected == 1:
        print(spanish())
    elif option_selected == 2:
        print(english())
    elif option_selected == 3:
        print(portuguese())
    elif option_selected == 4:
        print(french())
    elif option_selected == 5:
        exit()
    else:
        print(incorrect_option())
