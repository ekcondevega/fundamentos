list1 = [3, -3, 7, 365, 24000.2]
print(list1)
print(list1[3])
print(list1[-1])
print(len(list1))
print('----------------------------------------------------------')
list2 = ['Katherine', 'CV', 28, 9, 2022]
print(list2)
print(list2[3])
print(list2[3-4])
list2[1] = 'Conde Vega'
print(len(list2))
print(list2)
print('----------------------------------------------------------')
vector1 = []
vector1.append(3)
vector1.append(2)
print(vector1)
vector1.sort()
print(vector1)
vector1.sort()
