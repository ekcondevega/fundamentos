# Ciclo while
"""
Ejecuta una acción hasta donde se cumpla cierta condición
No sé cuántas veces se ejecutará la acción
"""
print('Este programa lee un número e imprime en consola un consecutivo desde ese número hasta 10')
number = int(input('Ingrese un número\t'))
while number < 10:
    print(number)
    number = number + 1

# Ciclo for
"""
Ejecuta una acción en un rango determinado
Sé cuántas veces se ejecutará la acción
"""
print('\nEste programa imprime en consola la tabla de multiplicar del 2 hasta 10 usando la suma incremental')
for counter in range(0, 10, 2):
    counter += 2
    print(counter, end=' ')

print('\n')
print('Este programa imprime en consola la palabra Programming separada por caracteres dejando entre cada uno un '
      'espacio en blanco')
for character in 'Programming':
    print(character, end=' ')
