number = int(input('Ingrese el número de estudiantes para los que va a ingresar 3 notas y obtener el promedio\n'))
for n in range(1, number+1):
    name = input(f'Ingrese el nombre del estudiante {n}\n')
    grades = 0
    for m in range(1, 4):
        grade = float(input(f'Ingrese la nota {m}\n'))
        grades += grade
        if m == 3:
            print(f'El promedio del estudiante {name} es {grades/m}')
