number = int(input('Ingrese un número'))
if number > 0:
    print(f'El número {number} es positivo')
    if number % 2 == 0:
        print(f'El número {number} es par')
    else:
        print(f'El número {number} es impar')
else:
    print(f'El número {number} es negativo')