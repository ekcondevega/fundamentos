"""
Operadores algebraicos
print(x > y)  # Mayor que. Evalúo si x es mayor que y
print(x < y) # Menor que. Evalúo si x es menor que y
print(x >=  y) # Mayor o igual que. Evalúo si x es mayor o igual que y
print(x <= y) # Menor o igual que. Evalúo si x es menor o igual que y
print(x == y) # Evalúo si x es igual a y
print(x != y) # Evalúo si x es diferente de y
"""

# Decision making with if statement
# Ejercicio: Pedir al usuario dos números y evaluar los 6 casos de operadores algebraicos
print('Este programa evalúa dos números y devuelve si cumple alguna de las 6 condiciones de operadores algebraicos\n>, '
      '<, >=, <=, == o !=')
number_1 = int(input('Por favor ingrese el primer valor\n'))
number_2 = int(input('Por favor ingrese el segundo valor\n'))
if number_1 > number_2:
    print('El número ', number_1, 'es mayor que', number_2)
if number_1 < number_2:
    print('El número ', number_1, 'es menor que', number_2)
if number_1 >= number_2:
    print('El número ', number_1, 'es mayor o igual que', number_2)
if number_1 <= number_2:
    print('El número ', number_1, 'es menor o igual que', number_2)
if number_1 == number_2:
    print('El número ', number_1, 'es igual a', number_2)
if number_1 != number_2:
    print('El número ', number_1, 'es diferente a', number_2)