# Definir la lista vacía
list = []

# Leer 10 números para agregarlos a la lista
for n in range(1, 11):
    number = int(input(f'Ingrese el número {n}\t'))
    list.append(number)

# Mostramos los elementos de la lista
print(list)

# Recorrer la lista
sum = 0
for n in range(0, 10):
    # Si el índice es par, muestro el elemento
    if n % 2 == 0:
        print(list[n], end=" ")
    # Si el índice es impar, sumo el elemento
    elif n % 2 != 0:
        sum += list[n]
print(f'\nLa suma de los elementos de las posiciones impares es {sum}')