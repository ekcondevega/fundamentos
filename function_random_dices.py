import random
"""
Lanzar un (1) dado 10 veces y contar la frecuencia de las caras con números pares y la frecuencia de las caras con 
números impares.
Lanzar dos (2) dados 10 veces y contar las veces en que la cara de los dos dados fue un número par.
Lanzar dos (2) dados 20 veces y contar las veces en que la cara de los dos dados es el mismo número.
Lanzar dos (2) dados 20 veces y contar las veces en que la cara de los dos dados es par y es el mismo número.
"""

# 1
print('Lanzar un (1) dado 10 veces y contar la frecuencia de las caras con números pares y la frecuencia de las caras'
      ' con números impares')
freq_even = 0
freq_odd = 0
faces = []
for roll in range(10):
    face = random.randrange(1, 7)
    faces.append(face)
    if face % 2 == 0:
        freq_even += 1
    else:
        freq_odd += 1
print(faces)
print(f'Frecuencia caras pares: {freq_even}\nFrecuencia caras impares: {freq_odd}')

# 2
print('Lanzar dos (2) dados 10 veces y contar las veces en que la cara de los dos dados fue un número par')
dice1 = []
dice2 = []
freq_two_even = 0
for roll in range(10):
    face1 = random.randrange(1, 7)
    dice1.append(face1)
    face2 = random.randrange(1, 7)
    dice2.append(face2)
    if face1 % 2 == 0 and face2 % 2 == 0:
        freq_two_even += 1
print(f'Dado 1: {dice1}\nDado 2: {dice2}')
print(f'Frecuencia en que ambos dados tuvieron cara par: {freq_two_even}')

# 3
print('Lanzar dos (2) dados 20 veces y contar las veces en que la cara de los dos dados es el mismo número')
dice1 = []
dice2 = []
freq_two_the_same = 0
for roll in range(20):
    face1 = random.randrange(1, 7)
    dice1.append(face1)
    face2 = random.randrange(1, 7)
    dice2.append(face2)
    if face1 == face2:
        freq_two_the_same += 1
print(f'Dado 1: {dice1}\nDado 2: {dice2}')
print(f'Frecuencia en que ambos dados tuvieron la misma cara: {freq_two_the_same}')

# 4
print('Lanzar dos (2) dados 20 veces y contar las veces en que la cara de los dos dados es par y es el mismo número')
dice1 = []
dice2 = []
freq_two_even_and_the_same = 0
for roll in range(20):
    face1 = random.randrange(1, 7)
    dice1.append(face1)
    face2 = random.randrange(1, 7)
    dice2.append(face2)
    if face1 % 2 == 0 and face2 % 2 == 0:
        if face1 == face2:
            freq_two_even_and_the_same += 1
print(f'Dado 1: {dice1}\nDado 2: {dice2}')
print(f'Frecuencia en que ambos dados tuvieron cara par y es la misma cara: {freq_two_even_and_the_same}')