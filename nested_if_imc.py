weight = float(input('Ingrese su peso\n'))
height = float(input('Ingrese su altura en metros (incluya decimales)\n'))
imc = weight/(height**2)
if weight > 0 and height > 0:
    print('Ingresaste un peso y una altura válida')
    if imc < 18.5:
        print(f'Tu IMC es {imc}, esto significa que tienes bajo peso')
    elif imc >= 18.5 and imc <= 24.9:
        print(f'Tu IMC es {imc}, esto significa que tienes un peso normal')
    elif imc >= 25:
        print(f'Tu IMC es {imc}, esto significa que tienes sobrepeso')
else:
    print('No ingresaste datos válidos')

