# Constantes
# Su valor no cambia durante la ejecución del programa
pi = 3.141592654

# Variables
"""
Espacio o campo en memoria que puede almacenar un valor y que puede ser modificado usando las instrucciones de un 
lenguaje de programación
"""
a = 4
a = 4 + 1
a = a + 5

# Identificador
"""
El identificador es el nombre de la variable. Puede contener letras, dígitos y raya al piso (_)
El identificador NO puede empezar con un dígito
Python es case sensitive, number es diferente de Number
"""
number = 1
Number = 2

# Las variables se clasifican de acuerdo con los tipos de datos que pueden almacenar
x = 7 # Esta variable almacena un tipo de dato entero
y = 3.5 # Esta variable almacena un tipo de dato flotante
# La función type(variable) permite conocer el tipo de dato de esa variable
type(x) # int
y = 10.5
type(y) # float

# Operadores aritméticos
add = x + y # operador de suma "+"
subtract = x - y  # operador de resta "-"
multiply = x * y # operador de multiplicación "*"
raise_x_to_y = x ** y # operador de exponenciación "**". Eleva x a la y
divide = x / y # operador de división "/". Python aplica la lógica matemática. División por cero no existe.
divide_floor = x // y # operador de división piso "//".
# Este operador elimina el decimal. Si el resultado es 3.5, con este operador se obtiene 3
x_module_y = x % y # operador de módulo (residuo de una división).

# Linealización de operaciones y priorización
# Linealizar es escribir una expresión algebraica en una sola línea.
"""
Python aplica los operadores aritméticos de acuerdo con las siguientes reglas de prioridad del operador 
(rules of operator precedence).
1. Las expresiones entre paréntesis se evalúan primero. 
Los paréntesis tienen el mayor nivel de prioridad. Las expresiones con paréntesis anidados, como (a / (b – c)), 
evalúan primero el paréntesis interior. 
2. Se evalúan en segundo lugar las operaciones de exponenciación. Si hay varias operaciones de exponenciación, 
Python las aplica de derecha a izquierda. 
3. Seguidamente se evalúan las operaciones de multiplicación, división y módulo (tienen el mismo nivel de prioridad). 
Si en una expresión hay varias operaciones de las mencionadas, Python las aplica de derecha a izquierda.
"""
a = 4
b = 2
c = 5
d = 1
exercise_1 = (a+(b/c))/((a/b)+c)
exercise_2 = (a+b+(a/b))/c
exercise_3 = (a/(a+b))/(a/(a-b))
exercise_4 = (a+(a/(a+b+(b/c))))/(a+(b/(c+a)))
exercise_5 = (a+b+c)/(a+(b/c))
exercise_6 = (a+b+(c/(d*a)))/(a+b*(c/d))
exercise_7 = (a+(b/c)+d)/a
exercise_8 = ((a/b)+(b/c))/((a/b)-(b/c))
exercise_9 = a + ((a+((a+b)/(c+d)))/(a+(a/b)))
exercise_10 = a+b+(c/d)+((a/(b-c))/(a/(b+c)))

print('a = 4\nb = 2\nc = 5\nd = 1', '\nEjercicio 1 = ', exercise_1, '\nEjercicio 2 = ', exercise_2,  '\nEjercicio 3 = ',
      exercise_3, '\nEjercicio 4 = ', exercise_4, '\nEjercicio 5 = ', exercise_5, '\nEjercicio 6 = ', exercise_6,
      '\nEjercicio 7 = ', exercise_7, '\nEjercicio 8 = ', exercise_8, '\nEjercicio 9 = ', exercise_9,
      '\nEjercicio 10 = ', exercise_10)
