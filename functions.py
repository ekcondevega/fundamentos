nombre = "Ana"

def saludar():
    print(f'Hello {nombre}')

def saludarte():
    print(f'Hello {nombre}')


def mensaje_saludo(nombre):
    return (f'Hello {nombre}')


def mensaje_saludo_completo(nombre, apellido):
    return (f'Hello {nombre} {apellido}')


saludar()
saludarte('Katherine')
print(mensaje_saludo('Katherine'))
print(mensaje_saludo_completo('Katherine', 'Conde'))